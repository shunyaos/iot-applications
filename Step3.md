# Build a Team 

We understand that to achieve great things we need great teams. Lets understand 
how to build good teams and build one for the product that you will be building.

## Roles
### Team Leaders 
1. Your chosen the problem statement need to pitched to your team members.
2. You need to put all your pitches in the Team leaders pitch issue.
3. Follow this template to from Teams. [TeamFormationTemplate-TechTeam](best_practices/TeamFormationTemplate-TechTeam.pdf)

> **Note** : Limit the size of the Teams to 3 persons or less.

## For Team Members 
1. You need to listen to the pitches given by the Team Leaders.
2. Decide which team you want to get into.
3. Pitch to your Team Leaders that you have the skills to get into the Team.


That is it! Done. You have completed This step now you can move on to the next 
step.

Checkout the Next step --> [Choose sensors and boards](Step4.md) . 


----

Umm.. I have some questions about ... 

For questions checkout our [FAQ](faq.md).

# A Guide to writing a Blog 

## Pre-requsites 
1. Knows Markdown markup language.
2. Good English writing skills  

**Note**: This whole guide assumes that you know how to write in Markdown language.

To get familiar with Markdown, 
1. Take this tutorial, [here](https://www.markdowntutorial.com).

## Steps to Writing a good blog

**This guide helps you write Tutorial Blogs**.


### Step 1: Write Heading & Introduction
Heading is the most important part of a blog.
Heading should be catchy, mysterious or clickbait to increase the views.

Introduction is a small summary of what the whole tutorial is all about.

### Step 2: Writing the Body of the blog
The body of the Blog can be written in many different ways. 

#### Innovation Blog format
If you are writing the blog in the Innovation format then the blog should have this flow.
1. Define the Problem
2. Tell more about the solution 
3. Describe the Features of the product
4. Competitive Analysis of similar products 
5. Team 

#### Tutorial Format 

If you are writing the tutorial, then the tutorial should have this flow.

1. Supplies used for the product.
2. Install shunya OS and Install shunya Interfaces
3. Introduction on GPIO or I2C or SPI etc, interfaces getting used
4. Connections / Building the circuit or connecting sensors
5. Introduction to shunya interfaces (introduce API)
6. Compiling code ( instructions on how to compile )
7. Running code (running instructions)


### Step 3: Beautify your Blog

**Pro-Tips**: 
1. Adding more and more photos will make your blog look really good. So communicate over diagrams than text.
2. Highlight key points and stress on the key things that your product solves.

#### How to take good photos (general)
1. Make sure there is a lot of sunlight while taking photos
2. If possible use a stand to take the photo
3. Re-arrange the sensors, boards or display instead of using camera zoom


#### Connections & Taking Photos (for Tutorial)
For this step execute the below instructions in this order
1. Arrange the Board and sensors on the Table. 
2. Take photos of the arrangement.
3. Connect the sensors to the Raspberry Pi (board)
4. Take photos of the connections
5. Power on the Raspberry Pi 
6. Compile the written code (the code you wrote in the Step 4)
7. Run the code, Make sure it is running
8. Take photo of the output. (output can be on screen or can be on the sensor)

#### Draw Fritzing Diagram (for Tutorial)
Fritzing is a software which can be used to draw awesome circuit diagrams, containing Raspberry Pi 3. It has many sensors, boards (RPI, arduino) pre-built into it, making it easier for us to create circuit diagrams.

to Download fritzing on Ubuntu 16.04 run command
```
sudo apt install fritzing
``` 

#### Publish Code on the blog (for Tutorial)

Tips for this step:
1. Code should be well documented 
2. Code should work well.

 
#### Step 4: Keywords and SEO
1. Keywords are like #tags (hashtags in instragram) putting right keywords will make the blog show up first on google search. 
2. For keywords use google Adwords to generate correct keywords.


## TODO : This is not a very refined Blog Template, If somebody knows how to write Blogs then please improve this template.



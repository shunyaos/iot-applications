# Interfaces Internship Stage 3

**Goal** : To solve industry problems by creating IoT products and showcase it 
to the world by blogging, creating videos and enrolling it into contests. 

**Pre-requisites** :
1. Team player 
2. Knows how to use Linux
3. Knows Basic C
4. Passion to solve problems and passion for IoT technology.

## Steps to Follow  

1. [Choose problem statements to solve](Step1.md)
2. [Design the product](Step2.md)
3. [Build a Team](Step3.md) 
4. [Work on the product](Step4.md)
4. [Show it to the world](Step5.md).  

# Design the product on Paper (brochure)

Now that you have decided on the problem statement it is time to get into the details 
and iron out the specifics.

## Choosing Sensors and Boards

You and your team is free to choose any sensors that you want to use in your 
project. And you are free to use any boards that we support.

1. [List of sensors](https://docs.google.com/spreadsheets/d/1EGli--jQHnjzumjDQ7cRg3Z932o9N0ma3w4p0H5lrdA/edit#gid=0) 
2. [List of all dev boards](https://docs.google.com/spreadsheets/d/1EGli--jQHnjzumjDQ7cRg3Z932o9N0ma3w4p0H5lrdA/edit#gid=1348085202)

that we have in our office. 

>Note: Opening these links on mobile then change the sheet respectively to view the correct list. 


**Once you have designed the product**
1. Write a summary your Problem statements [here](https://docs.google.com/spreadsheets/d/16KwxpSUyfzg5qEgI6olBLmGbj3pCXljSRdn5roY4LV8/edit#gid=774475265)
2. Fill the details of your Product [here](https://docs.google.com/spreadsheets/d/16KwxpSUyfzg5qEgI6olBLmGbj3pCXljSRdn5roY4LV8/edit#gid=129332003)
3. Create a new issue in this project.
4. Fill out the issue template in the description.


## Ordering Hardware that are not available 
If you have selected sensors/boards that are not available at our office 

1. Goto this [Link](https://docs.google.com/spreadsheets/d/1EGli--jQHnjzumjDQ7cRg3Z932o9N0ma3w4p0H5lrdA/edit#gid=354898439) 
1. Fill out the sheet, List all the sensors that you want ordered.
1. Provide us with a link of the sensor from `robu.in`
1. Don't forget to fill out the `Issue number`. `Issue number` is the unique number assigned to the issue when you have created it in [Step1](Step1.md). 

1. Your order will be verified.
2. Availability of these sensors will be checked. 
3. Sensors will be ordered according to the timeline (We order every Tue-Wed, takes 3-4 days from order date).
4. You can check the status of your order [here](https://docs.google.com/spreadsheets/d/1EGli--jQHnjzumjDQ7cRg3Z932o9N0ma3w4p0H5lrdA/edit#gid=354898439)
5. You can start with the project once the sensors arrive.


## For sensors whose status is "Not Ported" 
If you have selected a sensor which is not ported to the Shunya Interfaces library then you need to wait till it gets ported. 

Check the status of the porting [here](https://docs.google.com/spreadsheets/d/1EGli--jQHnjzumjDQ7cRg3Z932o9N0ma3w4p0H5lrdA/edit#gid=354898439)

That is it! Done. You have completed Step 2.

Checkout the Next step --> [Make](Step3.md).

----

Umm.. I have some questions about ... 

For questions checkout our [FAQ](faq.md).

# Show your application to the World  

Now that you have a cool application, show it to the world. 

To show it to the world, we have solved most of the things for you.

If you write your blog in Markdown format then we can post it on 3 platforms
1. Github 
2. Gitlab 
3. Medium 
4. IoTIoT Blog

You an also use these Medium to tell the world about your creation.
1. Create a video and upload it on YouTube.
2. Put pics on Instagram
3. Put pictures and video on LinkedIn etc. 

## Best practices 
We have best practices to follow when writing a blog so that it has impact on the audience. 

1. [Guide to writing a Blog in Markdown](best_practices/Blog.md)

Once you have done and posted your blog, then you can share it on your social media for more impact.

## Steps to complete
1. Provide us with the links for your blog, video or post inside the issue created in [Step1](Step1.md). 
So that we can promote it through our social media channels.


That is it! Done. You have completed the Internship. 
Want to create another product then you can follow these Steps again.

Umm.. I have some questions about ... 

For questions checkout our [FAQ](faq.md).
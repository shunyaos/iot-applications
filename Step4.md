# Work 

Now that you have all your raw materials ready, its time to execute.

## Leader Role   
1. Define your team's Goal and objectives 
2. Divide your team's Goal into tasks 
3. Divide these tasks into your team and assign roles to each one of your team members

We have a Template for Planning a task, [click here for the template](best_practices/Task_Template.pdf)

## Member Role 
1. Understand your Goal, Roles and responsibilities.
2. Execute all your tasks.
3. Document these Tasks inside the issue created in [Step1](Step1.md).


## Resources to Execute 
1. Shunya Interfaces Docs and API - [Link](https://shunyaos.github.io/Shunya-Interfaces/)
1. Send SMS and Alerts - [Link](https://www.twilio.com/docs/sms/whatsapp/quickstart/curl?code-sample=code-send-a-message-with-whatsapp-and-curl&code-language=curl&code-sdk-version=json)
1. Create Dashboard using Grafana - [Link](https://grafana.com/docs/grafana/latest/guides/getting_started/)
1. Create Dashboard using ThingsBoard [Link](https://thingsboard.io/docs/getting-started-guides/helloworld/)


That is it! Done. You have completed Step 4.

Checkout the Next step --> [Show it to the world](Step5.md) .

Umm.. I have some questions about ... 

For questions checkout our [FAQ](faq.md).

# Choose Problem Statements to solve 

Choose **Multiple** problem statements to solve from the pool of problem statements, 
Links for pool of industry validated problem statements given below. 

1. [Smart India Hackathon](https://www.sih.gov.in/sih2020PS/QWxs/QWxs/QWxs/SW5kdXN0cnlfcGVyb3Nvbm5lbA==)
2. [Startup India Problem Statements](https://www.startupindia.gov.in/content/sih/en/ams-application/application-listing.html)
3. [Plugin Industry Problem statements](best_practices/industry_problem_statements.pdf)
4. [GIH](http://ssipgujarat.in/gih/problem_statement.php)

## Why limit to these problem statements?
The idea behind this is that you choose problem statements that have **market validation**.
Since it is a lot of hard-work to validate from the market, we have decided to
instead take up problem statements which are given by the **industry**.

If you want to take up problem statement keep in mind, these **key points**
1. Problem statement should be **market validated** or **given out by the industry**.
2. Problem statement should be of **embedded domain**.


That is it! Done. You have completed Step 1.

Checkout the Next step --> [Choose sensors and boards](Step2.md).

----

Umm.. I have some questions about ... 

For questions checkout our [FAQ](faq.md).
